import React, { Component } from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import './App.css';
import Nav from "../../components/Nav/Nav";
import FilmList from "../../containers/FilmList/FilmList";
import JokesBuilder from "../../containers/JokesBuilder/JokesBuilder";

class App extends Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <div>
            <Route path='/' component={Nav}/>
            <Route path='/film-list' component={FilmList}/>
            <Route path='/jokes' component={JokesBuilder}/>
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
