import React from 'react';
import {Link} from 'react-router-dom';

const Nav = props => (
  <nav>
    <Link to="/film-list">Task 1</Link>
    <Link to="/jokes">Task 2</Link>
  </nav>
);

export default Nav;