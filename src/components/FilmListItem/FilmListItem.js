import React, {Component} from 'react';

class FilmListItem extends Component{

  shouldComponentUpdate(nextProps){
    return this.props.children === nextProps.children
  }

  render () {
    return <div>
      {this.props.children}
      <button onClick={this.props.click}>X</button>
    </div>;
  }
}

export default FilmListItem;
