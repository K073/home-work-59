import React, {Component} from 'react';
import FilmListItem from "../../components/FilmListItem/FilmListItem";

const LOCAL_STORAGE_STATE_NAME = 'FilmListState';

class FilmList extends Component {
  state = {
    filmList: [],
    value: ''
  };

  constructor (props) {
    super(props);
    this.state.filmList = JSON.parse(localStorage.getItem(LOCAL_STORAGE_STATE_NAME)) || [];
  }

  addFilm = event => {
    event.preventDefault();
    const ID = Date.now();
    const filmList = [...this.state.filmList];
    filmList.push({
      value: this.state.value,
      id: ID
    });

    this.setState({filmList});
  };

  removeFilm = id => {
    const filmList = [...this.state.filmList];
    const index = filmList.map(value => value.id).indexOf(id);
    filmList.splice(index, 1);

    this.setState({filmList})
  };

  inputHandler = event => {
    this.setState({value: event.target.value});
  };

  componentDidUpdate() {
    localStorage.setItem(LOCAL_STORAGE_STATE_NAME, JSON.stringify(this.state.filmList));
  }

  render() {
    return (
      <div>
        <form onSubmit={this.addFilm}>
          <input type="text" value={this.state.value} onChange={this.inputHandler} placeholder={'Enter Film Name'}/>
          <input type="submit" value="Add"/>
        </form>
        {this.state.filmList.length > 0 ? this.state.filmList.map((value) => <FilmListItem key={value.id} click={() => this.removeFilm(value.id)}>{value.value}</FilmListItem>) : <p>add films!</p>}
      </div>
    );
  }
}

export default FilmList;