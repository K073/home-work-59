import React, {Component} from 'react';

class JokesBuilder extends Component {
  state = {
    jokesList: []
  };


  generateJokes = count => {
    const promiseList = [];
    const jokesList = [];
    for (let i = 0; i < count; i++) {
      promiseList.push(fetch('https://api.chucknorris.io/jokes/random').then(value => value.json()).then(value => {
        jokesList.push({
          value: value.value,
          id: value.id
        })
      }))
    }

    Promise.all(promiseList).then(() => {
      this.setState({jokesList});
    })
  };

  componentDidMount() {
      this.generateJokes(5);
  }

  render() {
    return <div>
      {this.state.jokesList.map(value => <div key={value.id}>{value.value}</div>)}
    </div>
  }
}


export default JokesBuilder;